import { BasePage } from './common/BasePage';
import { marketPlacePageElements } from '../elements/marketPlacePageElements';

export class MarketPlacePage extends BasePage {
    constructor() {
        super(marketPlacePageElements.url);
    }
}
