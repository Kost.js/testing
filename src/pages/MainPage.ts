import { BasePage } from './common/BasePage';
import { mainPageElements } from '../elements/mainPageElements';

export class MainPage extends BasePage {
    constructor(_langVer = 'nl-nl/') {
        super(`${mainPageElements.url}${_langVer}`); // mainPage directs to Holland version by default
    }
}

