import { BasePage } from './common/BasePage';
import { MainPage } from "./MainPage";
import { StartPage } from './StartPage';
import { MarketPlacePage } from './MarketPlacePage';
import { ICountry } from "../interfaces/ICountry";
import {  globalPageElements } from '../elements/globalPageElements';

export class GlobalPage extends BasePage {
    constructor() {
        super(globalPageElements.url, globalPageElements.loaderImage);
    }
    /**
     * @method clicks Logo in-link directed to Start Page and returns instance of StartPage
     */
    public clickLogo(): StartPage {
        this.waitForElement(globalPageElements.logoLinkToStartPage);
        $(globalPageElements.logoLinkToStartPage).click();
        return new StartPage();
    }
    /**
     * @method clicks link to MarketPlacePage and returns its instance
     */
    public clickDealerLogin(): MarketPlacePage {
        this.waitForElement(globalPageElements.dealerLoginLink);
        const numberOfOpenWindows = browser.getWindowHandles().length;
        $(globalPageElements.dealerLoginLink).click();
        this.waitUntilNewWindowOpen(numberOfOpenWindows);
        return  new MarketPlacePage().switchToNewWindow();
    }
    /**
     * @method returns object with two methods than allow to open Country picker
     */
    public clickChooseYourCountry(): { byLink: () => ICountry; byButton: () => ICountry } {
        return {
         byLink: () => this.chooseCountry(globalPageElements.chooseYourCountryLink),
         byButton: () => this.chooseCountry(globalPageElements.chooseYouCountyBtn)
        }
    }
    /**
     * @method clicks link or button that direct to country panel
     * @param selector
     */
    private chooseCountry(selector: string): ICountry {
        $(selector).click();
        $(globalPageElements.countryPickingField).waitForDisplayed(2500);
        return this.getListOfCountries();
    }
    /**
     * @method returns object with methods for navigating to different language versions of site
     */
    private getListOfCountries(): ICountry {
        const listCountry = {} as ICountry;
        const countries = $$(globalPageElements.chooseCountryPanel.links)
            .map(a => a.getText()
                .replace(/\s/g, ''));
        countries.forEach((country, ind) => {
            listCountry[`pick${country}`] = () => {
                $$(globalPageElements.chooseCountryPanel.links)[ind].click();
                return new MainPage();
            }
        });
        listCountry['pickClose'] = () => {
          $(globalPageElements.chooseCountryPanel.close).click();
          return new GlobalPage();
        };
        return listCountry;
    }
}
