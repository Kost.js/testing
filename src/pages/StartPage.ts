import { BasePage } from './common/BasePage';
import { GlobalPage } from './GlobalPage';
import { MainPage } from './MainPage';
import { ILanguage } from '../interfaces/ILanguage';
import { startPageElements } from '../elements/startPageElements';

export class StartPage extends BasePage {
    constructor() {
        super(startPageElements.url);
    }
    /**
     * @method passes to GlobalPage and returns its instance
     */
    passToGlobalPage(): GlobalPage {
        browser.waitUntil(() =>  $(startPageElements.logo).isDisplayed(), 2500);
        const numberOfOpenWindows = browser.getWindowHandles().length;
        $(startPageElements.linkToGlobalPage).click();
        this.waitUntilNewWindowOpen(numberOfOpenWindows);
        return new GlobalPage().switchToNewWindow();
    }
    /**
     * @method returns object with method for working with language panel
     */
    getListOfLanguages(): ILanguage  {
        $(startPageElements.listCountries).waitForDisplayed(5000);
        let listLang = {} as ILanguage;
        const languageLink = $$(startPageElements.languageLink).map(el =>
            el.$(startPageElements.languageText).getText()
            .replace(/\s/g, '')
        );
        const languageBtn = $$(startPageElements.languageBtn)
            .map(el => el.$(startPageElements.languageText).getText()
                .replace(/\s/g, ''));
        languageLink.forEach((lang, ind) => {
            listLang[`passTo${lang}Version`] = () => {
                $$(startPageElements.languageLink)[ind].click();
                return new MainPage();
            };
        });
        languageBtn.forEach((lang, ind) => {
            listLang[`passTo${lang}Version`] = () => {
                $$(startPageElements.languageBtn)[ind].click();
                const langVersion =  $$(startPageElements.btnLangOptionLink)[ind].$$('a');
                let nameVersions = langVersion.map(lnk => lnk.getText());
                let obj: { [key: string]: Function } = {};
                nameVersions.forEach((lang, num) => obj[`pick${lang}`] = () => {
                    langVersion[num].click();
                    return new MainPage();
                });
                return obj;
            }
        });
        return listLang;
    }
}
