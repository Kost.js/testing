import { ILanguage } from '../../interfaces/ILanguage';
import { multiLangList } from '../../elements/multiLangList';
import { MainPage } from '../MainPage';

export class PageNavigator {
    public static chooseSiteVersion(): ILanguage {
        const listVersion = {} as ILanguage;
        for(let lang of Object.keys(multiLangList)) {
            listVersion[`passTo${lang}Version`] = (typeof multiLangList[lang] === 'string') ?
                () => new MainPage(multiLangList[lang].toString()).goToPage() :
                () => {
                    const obj: { [key: string]: Function } = {};
                    const restUrl = Object.values( multiLangList[lang] );
                    Object.keys( multiLangList[lang] ).forEach( (ver, ind) => {
                        obj[`pick${ver}`] = () => new MainPage(restUrl[ind]).goToPage();
                    } );
                    return obj;
                }
        }
        return listVersion
    }
}
