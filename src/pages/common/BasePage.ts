export class BasePage {
    private url: string;
    private fadeElement: string;
    constructor(_url: string, _fadeElement: string = '') {
        this.url = `https://www.${_url}`;
        this.fadeElement = _fadeElement;
    }
    /**
     * @method directs to pointed url and returns current page's instance
     */
    public goToPage(): this {
      browser.url(this.url);
      this.waitForElementFaded();
      return this;
    }
    /**
     * @method for switching to another opened browser's window according to the pass
     */
    public switchToNewWindow(): this {
        browser.switchWindow(this.url);
        this.waitForElementFaded();
        return this;
    }
    /**
     * @method returns string with page's title
     */
    public getPageTitle(): string {
        return browser.getTitle();
    }
    /**
     * @method returns string with page's url
     */
    public getPageUrl(): string {
        return browser.getUrl();
    }
    /**
     * @method waits for element will be appeared and displayed
     * @param elem
     * @param numb
     */
    protected waitForElement(elem: string, numb?: number ): void {
        const locator = (numb) ? $$(elem)[numb] : $(elem);
        locator.waitForExist(2500);
        locator.waitForDisplayed(2500);
    }
    /**
     *@method waits until newWindow will be opened
     */
    protected waitUntilNewWindowOpen(amountOfOpenWindBefore: number) {
        browser.waitUntil(() =>
            browser.getWindowHandles().length > amountOfOpenWindBefore
            , 5000);
    }
    /**
     * @method waits until element will be faded in case of page has this kind of element that disappeared through loading process
     */
    private waitForElementFaded(): void {
        if (this.fadeElement) {
            browser.waitUntil(
                () => $(this.fadeElement).getCSSProperty('opacity').value === 0,
                5000);
        }
        return;
    }
}

