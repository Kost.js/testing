export const expectResults = {
    urls: {
        startPage: `https://ww.carnext.com/`,
        globalPage: `https://www.carnext.com/global.html`,
        marketPlacePage: `https://www.carnextmarketplace.com/`,
        mainPage: {
            'Austria': `https://www.carnext.com/de-at/`,
            'CzechRepublic': `https://www.carnext.com/cs-cz/`,
            'Denmark': `https://www.carnext.com/da-dk/`,
            'Finland': `https://www.carnext.com/fi-fi/`,
            'France': `https://www.carnext.com/fr-fr/`,
            'Hungary': `https://www.carnext.com/hu-hu/`,
            'Ireland': `https://www.carnext.com/en-ie/`,
            'Italy': `https://www.carnext.com/it-it/`,
            'Netherlands': `https://www.carnext.com/nl-nl/`,
            'Norway': `https://www.carnext.com/nb-no/`,
            'Poland': `https://www.carnext.com/pl-pl/`,
            'Portugal': `https://www.carnext.com/pt-pt/`,
            'Romania': `https://www.carnext.com/ro-ro/`,
            'Slovakia': `https://www.carnext.com/sk-sk/`,
            'Spain': `https://www.carnext.com/es-es/`,
            'Sweden': `https://www.carnext.com/sv-se/`,
            'Turkey': `https://www.carnext.com/tr-tr/`,
            'UnitedArabEmirates': `https://www.carnext.com/en-ae/`,
            'UnitedKingdom': `https://www.carnext.com/en-gb/`,
            'Belgium': { 'English': `https://www.carnext.com/en-be/`, 'Français': `https://www.carnext.com/fr-be/`, 'Nederlands': `https://www.carnext.com/nl-be/` },
            'Germany': { 'Deutsch': `https://www.carnext.com/de-de/`, 'English': `https://www.carnext.com/en-de/` },
            'Greece': { 'English': `https://www.carnext.com/en-gr/`, 'Greek': `https://www.carnext.com/el-gr/` },
            'Luxembourg': { 'English': `https://www.carnext.com/en-lu/`, 'Français': `https://www.carnext.com/fr-lu/` }
        }
    },
    titles: {
        startPage: `What's next? | CarNext.com`,
        globalPage: 'CarNext.com by Leaseplan | Global',
        marketPlacePage: `Homepage - CarNext Marketplace`,
        mainPage: {
            'Austria': 'Gebrauchtwagen | Österreich | CarNext.com',
            'CzechRepublic': '',
            'Denmark': '',
            'Finland': 'Käytetyt Autot | Suomi | CarNext.com',
            'France': '',
            'Hungary': 'Használt Autók | Magyarország | CarNext.com',
            'Ireland': '',
            'Italy': '',
            'Netherlands': 'Occasions | Nederland | CarNext.com',
            'Norway': '',
            'Poland': '',
            'Portugal': '',
            'Romania': 'Autovehicule Rulate | România | CarNext.com',
            'Slovakia': '',
            'Spain': '',
            'Sweden': '',
            'Turkey': '',
            'UnitedArabEmirates': '',
            'UnitedKingdom': ``,
            'Belgium': { 'English': ``, 'Français': 'Tweedehands Auto\'s | België | CarNext.com', 'Nederlands': '' },
            'Germany': { 'Deutsch': '', 'English': '' },
            'Greece': { 'English':'', 'Greek': '' },
            'Luxembourg': { 'English': '', 'Français': '' }
        }
    }
};

