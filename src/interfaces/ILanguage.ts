import { MainPage } from '../pages/MainPage';
export interface ILanguage {
        passToAustriaVersion: () => MainPage,
        passToCzechRepublicVersion: () => MainPage,
        passToDenmarkVersion: () => MainPage,
        passToFinlandVersion: () => MainPage,
        passToFranceVersion: () => MainPage,
        passToHungaryVersion: () => MainPage,
        passToIrelandVersion: () => MainPage,
        passToItalyVersion: () => MainPage,
        passToNetherlandsVersion: () => MainPage,
        passToNorwayVersion: () => MainPage,
        passToPolandVersion: () => MainPage,
        passToPortugalVersion: () => MainPage,
        passToRomaniaVersion: () => MainPage,
        passToSlovakiaVersion: () => MainPage,
        passToSpainVersion: () => MainPage,
        passToSwedenVersion: () => MainPage,
        passToTurkeyVersion: () => MainPage,
        passToUnitedArabEmiratesVersion: () => MainPage,
        passToUnitedKingdomVersion: () => MainPage,
        passToBelgiumVersion: () => { pickEnglish: () => MainPage, pickFrançais: () => MainPage, pickNederlands: () => MainPage },
        passToGermanyVersion: () => { pickDeutsch: () => MainPage, pickEnglish: () => MainPage },
        passToGreeceVersion: () =>  { pickEnglish: () => MainPage, pickGreek: () => MainPage },
        passToLuxembourgVersion: () =>  { pickEnglish: () => MainPage, pickFrançais: () => MainPage },
        [key: string]: Function
};
