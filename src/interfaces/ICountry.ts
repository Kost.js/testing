import { MainPage } from '../pages/MainPage';
import { GlobalPage } from '../pages/GlobalPage';

export interface ICountry {
    pickAustria: () => MainPage,
    pickBelgium: () => MainPage,
    pickCzechRepublic: () => MainPage,
    pickFinland: () => MainPage,
    pickFrance: () => MainPage,
    pickGreece: () => MainPage,
    pickHungary: () => MainPage,
    pickIreland: () => MainPage,
    pickNetherlands: () => MainPage,
    pickNorway: () => MainPage,
    pickPoland: () => MainPage,
    pickPortugal: () => MainPage,
    pickRomania: () => MainPage,
    pickSlovakia: () => MainPage,
    pickSpain: () => MainPage,
    pickSweden: () => MainPage,
    pickTurkey: () => MainPage,
    pickUnitedKingdom: () => MainPage,
    pickItaly: () => MainPage,
    pickDenmark: () => MainPage,
    pickGermany: () => MainPage,
    pickLuxemburg: () => MainPage,
    closePanel: () => GlobalPage,
    [key: string]: Function
}
