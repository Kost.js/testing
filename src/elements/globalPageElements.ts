export const globalPageElements = {
    url: 'carnext.com/global.html',
    loaderImage: '.preloader',
    logoLinkToStartPage: '.header__logo-image',
    toStartPageLink: '.header__inner a:nth-child(1)',
    dealerLoginLink: '.header__inner a:nth-child(2)',
    chooseYourCountryLink: '.header__inner a:nth-child(3)',
    chooseYouCountyBtn: '.intro__choose-country-button',
    countryPickingField: '.country-menu__content',
    chooseCountryPanel: {
        links: '.country-menu__link',
        close: 'a.country-menu__close'
    }
};
