export const startPageElements = {
    url: 'carnext.com/',
    logo: 'img[alt="CarNext"]',
    linkToGlobalPage: 'a.link',
    languageLink: 'a.locale-selector__link',
    languageBtn: '.locale-selector__button',
    btnLangOptionLink: '.locale-selector__languages',
    languageText: 'span.flag-icon__label',
    listCountries: '.locale-selector__countries'
};

