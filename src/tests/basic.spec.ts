import { StartPage } from '../pages/StartPage';
import { MarketPlacePage } from '../pages/MarketPlacePage';
import { GlobalPage } from '../pages/GlobalPage';
import { PageNavigator } from '../pages/common/PageNavigator';
import { expectResults } from '../testData/expectResults'

describe(`Just trial test`, () => {
    it(`Should be passed to English version of German's main page via Start page`, () => {
         const page = new StartPage().goToPage().getListOfLanguages().passToGermanyVersion().pickEnglish();
         expect(page.getPageUrl()).toEqual(expectResults.urls.mainPage.Germany.English);
     });

     it(`Should be passed to French version of Belgium's main page directly`, () => {
         const mainPage = PageNavigator.chooseSiteVersion().passToBelgiumVersion().pickFrançais();
         expect(mainPage.getPageUrl()).toEqual(expectResults.urls.mainPage.Belgium.Français);
     });

     it(`Should be passed to Global page via Start page`, () => {
         const globalPage = new StartPage().goToPage().passToGlobalPage();
         expect(globalPage.getPageUrl()).toEqual(expectResults.urls.globalPage);
     });

     it(`Should be passed to Market place page directly`, () => {
         const marketPage = new MarketPlacePage().goToPage();
         expect(marketPage.getPageUrl()).toEqual(expectResults.urls.marketPlacePage);
     });

     it(`Should be passed to Market place page via Start and Global pages`, () => {
         const marketPage = new StartPage().goToPage().passToGlobalPage().clickDealerLogin();
         expect(marketPage.getPageUrl()).toEqual(expectResults.urls.marketPlacePage);
     });

     it(`Should be passed to Market place page directly `, () => {
         const marketPage = new MarketPlacePage().goToPage();
         expect(marketPage.getPageUrl()).toEqual(expectResults.urls.marketPlacePage);
     });

    it(`Should be passed to Global page directly`, () => {
        const globalPage = new GlobalPage().goToPage();
        expect(globalPage.getPageUrl()).toEqual(expectResults.urls.globalPage);
    });
});
