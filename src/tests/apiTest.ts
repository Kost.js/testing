import { SwapiTest } from '../api/SwapiTest';
import { expectApiData } from '../api/expectApiData';

describe(`Try to get data from api`, () => {
  it(`First example`, async () => {
      const actualResult = await SwapiTest.getAllResources()
      expect(actualResult).toEqual(expectApiData.resources);
  });

  it('Second example', async () => {
      const actualResult = await SwapiTest.getAllPeople();
      expect(actualResult).toContain(expectApiData.people.nameOwenLars);
  });
});
