export const expectApiData = {
    resources: {
        "films": "https://swapi.co/api/films/",
        "people": "https://swapi.co/api/people/",
        "planets": "https://swapi.co/api/planets/",
        "species": "https://swapi.co/api/species/",
        "starships": "https://swapi.co/api/starships/",
        "vehicles": "https://swapi.co/api/vehicles/"
    },
    people: {
       nameOwenLars: {
           "name": "Owen Lars",
           "height": "178",
           "mass": "120",
           "hair_color": "brown, grey",
           "skin_color": "light",
           "eye_color": "blue",
           "birth_year": "52BBY",
           "gender": "male",
           "homeworld": "https://swapi.co/api/planets/1/",
           "films": [
               "https://swapi.co/api/films/5/",
               "https://swapi.co/api/films/6/",
               "https://swapi.co/api/films/1/"
           ],
           "species": [
               "https://swapi.co/api/species/1/"
           ],
           "vehicles": [],
           "starships": [],
           "created": "2014-12-10T15:52:14.024000Z",
           "edited": "2014-12-20T21:17:50.317000Z",
           "url": "https://swapi.co/api/people/6/"
      }
    }
};
