import axios from 'axios';
const apiData = require('./links.json');
/**
 * @class for API testing Swapi.co
 */
export class SwapiTest {
    static async getAllResources():Promise<Object | undefined> {
        try {
          const response = await axios.get(apiData.allData);
          return response.data;
        }
        catch (e) {
            console.log(e);
        }
    }
    /**
     * @method returns array with all people
     */
    static async getAllPeople(): Promise<Object [] | undefined> {
        try {
            return SwapiTest.getAllPages(apiData.allPeople);
        }
        catch (e) {
            console.log(e);
        }
    }
    /**
     * @method returns array with objects from all pages recursively
     * @param url
     */
    private static async getAllPages(url: string): Promise<[]> {
        const response = await axios.get(url);
        const arr = response.data.results;
        if (response.data.next === null) return arr;
        return arr.concat(await SwapiTest.getAllPages(response.data.next));
    }
}
