# Trial Project

Description of base command of framework

## For quick start

Firstly, run this command for installing all necessary modules:

```
npm install
```

## NPM script commands: 

* For installing and starting selenium standalone run:
```
npm start
```
* For launching tests (on firefox by default) run:
```
npm test
```